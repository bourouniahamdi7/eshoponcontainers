package org.gradle.accessors.dm;

import org.gradle.api.NonNullApi;
import org.gradle.api.artifacts.MinimalExternalModuleDependency;
import org.gradle.plugin.use.PluginDependency;
import org.gradle.api.artifacts.ExternalModuleDependencyBundle;
import org.gradle.api.artifacts.MutableVersionConstraint;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.internal.catalog.AbstractExternalDependencyFactory;
import org.gradle.api.internal.catalog.DefaultVersionCatalog;
import java.util.Map;
import javax.inject.Inject;

/**
 * A catalog of dependencies accessible via the `libs` extension.
*/
@NonNullApi
public class LibrariesForLibs extends AbstractExternalDependencyFactory {

    private final AbstractExternalDependencyFactory owner = this;
    private final AxonLibraryAccessors laccForAxonLibraryAccessors = new AxonLibraryAccessors(owner);
    private final BenLibraryAccessors laccForBenLibraryAccessors = new BenLibraryAccessors(owner);
    private final CommonsLibraryAccessors laccForCommonsLibraryAccessors = new CommonsLibraryAccessors(owner);
    private final DgsLibraryAccessors laccForDgsLibraryAccessors = new DgsLibraryAccessors(owner);
    private final HibernateLibraryAccessors laccForHibernateLibraryAccessors = new HibernateLibraryAccessors(owner);
    private final Junit5LibraryAccessors laccForJunit5LibraryAccessors = new Junit5LibraryAccessors(owner);
    private final KafkaLibraryAccessors laccForKafkaLibraryAccessors = new KafkaLibraryAccessors(owner);
    private final LogstashLibraryAccessors laccForLogstashLibraryAccessors = new LogstashLibraryAccessors(owner);
    private final ReactorLibraryAccessors laccForReactorLibraryAccessors = new ReactorLibraryAccessors(owner);
    private final SpringLibraryAccessors laccForSpringLibraryAccessors = new SpringLibraryAccessors(owner);
    private final VersionAccessors vaccForVersionAccessors = new VersionAccessors(providers, config);
    private final BundleAccessors baccForBundleAccessors = new BundleAccessors(providers, config);
    private final PluginAccessors paccForPluginAccessors = new PluginAccessors(providers, config);

    @Inject
    public LibrariesForLibs(DefaultVersionCatalog config, ProviderFactory providers) {
        super(config, providers);
    }

        /**
         * Creates a dependency provider for flywaydb (org.flywaydb:flyway-core)
         * This dependency was declared in catalog libs.versions.toml
         */
        public Provider<MinimalExternalModuleDependency> getFlywaydb() { return create("flywaydb"); }

        /**
         * Creates a dependency provider for lombok (org.projectlombok:lombok)
         * This dependency was declared in catalog libs.versions.toml
         */
        public Provider<MinimalExternalModuleDependency> getLombok() { return create("lombok"); }

        /**
         * Creates a dependency provider for minio (io.minio:minio)
         * This dependency was declared in catalog libs.versions.toml
         */
        public Provider<MinimalExternalModuleDependency> getMinio() { return create("minio"); }

        /**
         * Creates a dependency provider for okhttp (com.squareup.okhttp3:okhttp)
         * This dependency was declared in catalog libs.versions.toml
         */
        public Provider<MinimalExternalModuleDependency> getOkhttp() { return create("okhttp"); }

        /**
         * Creates a dependency provider for pipelinr (an.awesome:pipelinr)
         * This dependency was declared in catalog libs.versions.toml
         */
        public Provider<MinimalExternalModuleDependency> getPipelinr() { return create("pipelinr"); }

        /**
         * Creates a dependency provider for postgresql (org.postgresql:postgresql)
         * This dependency was declared in catalog libs.versions.toml
         */
        public Provider<MinimalExternalModuleDependency> getPostgresql() { return create("postgresql"); }

    /**
     * Returns the group of libraries at axon
     */
    public AxonLibraryAccessors getAxon() { return laccForAxonLibraryAccessors; }

    /**
     * Returns the group of libraries at ben
     */
    public BenLibraryAccessors getBen() { return laccForBenLibraryAccessors; }

    /**
     * Returns the group of libraries at commons
     */
    public CommonsLibraryAccessors getCommons() { return laccForCommonsLibraryAccessors; }

    /**
     * Returns the group of libraries at dgs
     */
    public DgsLibraryAccessors getDgs() { return laccForDgsLibraryAccessors; }

    /**
     * Returns the group of libraries at hibernate
     */
    public HibernateLibraryAccessors getHibernate() { return laccForHibernateLibraryAccessors; }

    /**
     * Returns the group of libraries at junit5
     */
    public Junit5LibraryAccessors getJunit5() { return laccForJunit5LibraryAccessors; }

    /**
     * Returns the group of libraries at kafka
     */
    public KafkaLibraryAccessors getKafka() { return laccForKafkaLibraryAccessors; }

    /**
     * Returns the group of libraries at logstash
     */
    public LogstashLibraryAccessors getLogstash() { return laccForLogstashLibraryAccessors; }

    /**
     * Returns the group of libraries at reactor
     */
    public ReactorLibraryAccessors getReactor() { return laccForReactorLibraryAccessors; }

    /**
     * Returns the group of libraries at spring
     */
    public SpringLibraryAccessors getSpring() { return laccForSpringLibraryAccessors; }

    /**
     * Returns the group of versions at versions
     */
    public VersionAccessors getVersions() { return vaccForVersionAccessors; }

    /**
     * Returns the group of bundles at bundles
     */
    public BundleAccessors getBundles() { return baccForBundleAccessors; }

    /**
     * Returns the group of plugins at plugins
     */
    public PluginAccessors getPlugins() { return paccForPluginAccessors; }

    public static class AxonLibraryAccessors extends SubDependencyFactory {
        private final AxonKafkaLibraryAccessors laccForAxonKafkaLibraryAccessors = new AxonKafkaLibraryAccessors(owner);
        private final AxonSpringLibraryAccessors laccForAxonSpringLibraryAccessors = new AxonSpringLibraryAccessors(owner);

        public AxonLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for mongo (org.axonframework.extensions.mongo:axon-mongo)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getMongo() { return create("axon.mongo"); }

        /**
         * Returns the group of libraries at axon.kafka
         */
        public AxonKafkaLibraryAccessors getKafka() { return laccForAxonKafkaLibraryAccessors; }

        /**
         * Returns the group of libraries at axon.spring
         */
        public AxonSpringLibraryAccessors getSpring() { return laccForAxonSpringLibraryAccessors; }

    }

    public static class AxonKafkaLibraryAccessors extends SubDependencyFactory {
        private final AxonKafkaSpringLibraryAccessors laccForAxonKafkaSpringLibraryAccessors = new AxonKafkaSpringLibraryAccessors(owner);

        public AxonKafkaLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at axon.kafka.spring
         */
        public AxonKafkaSpringLibraryAccessors getSpring() { return laccForAxonKafkaSpringLibraryAccessors; }

    }

    public static class AxonKafkaSpringLibraryAccessors extends SubDependencyFactory {
        private final AxonKafkaSpringBootLibraryAccessors laccForAxonKafkaSpringBootLibraryAccessors = new AxonKafkaSpringBootLibraryAccessors(owner);

        public AxonKafkaSpringLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at axon.kafka.spring.boot
         */
        public AxonKafkaSpringBootLibraryAccessors getBoot() { return laccForAxonKafkaSpringBootLibraryAccessors; }

    }

    public static class AxonKafkaSpringBootLibraryAccessors extends SubDependencyFactory {

        public AxonKafkaSpringBootLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for starter (org.axonframework.extensions.kafka:axon-kafka-spring-boot-starter)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getStarter() { return create("axon.kafka.spring.boot.starter"); }

    }

    public static class AxonSpringLibraryAccessors extends SubDependencyFactory {
        private final AxonSpringBootLibraryAccessors laccForAxonSpringBootLibraryAccessors = new AxonSpringBootLibraryAccessors(owner);

        public AxonSpringLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at axon.spring.boot
         */
        public AxonSpringBootLibraryAccessors getBoot() { return laccForAxonSpringBootLibraryAccessors; }

    }

    public static class AxonSpringBootLibraryAccessors extends SubDependencyFactory {

        public AxonSpringBootLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for starter (org.axonframework:axon-spring-boot-starter)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getStarter() { return create("axon.spring.boot.starter"); }

    }

    public static class BenLibraryAccessors extends SubDependencyFactory {
        private final BenManesLibraryAccessors laccForBenManesLibraryAccessors = new BenManesLibraryAccessors(owner);

        public BenLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at ben.manes
         */
        public BenManesLibraryAccessors getManes() { return laccForBenManesLibraryAccessors; }

    }

    public static class BenManesLibraryAccessors extends SubDependencyFactory {

        public BenManesLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for caffeine (com.github.ben-manes.caffeine:caffeine)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCaffeine() { return create("ben.manes.caffeine"); }

    }

    public static class CommonsLibraryAccessors extends SubDependencyFactory {

        public CommonsLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for collections4 (org.apache.commons:commons-collections4)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCollections4() { return create("commons.collections4"); }

            /**
             * Creates a dependency provider for lang3 (org.apache.commons:commons-lang3)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getLang3() { return create("commons.lang3"); }

    }

    public static class DgsLibraryAccessors extends SubDependencyFactory {
        private final DgsCodegenLibraryAccessors laccForDgsCodegenLibraryAccessors = new DgsCodegenLibraryAccessors(owner);
        private final DgsExtendedLibraryAccessors laccForDgsExtendedLibraryAccessors = new DgsExtendedLibraryAccessors(owner);
        private final DgsSpringLibraryAccessors laccForDgsSpringLibraryAccessors = new DgsSpringLibraryAccessors(owner);

        public DgsLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at dgs.codegen
         */
        public DgsCodegenLibraryAccessors getCodegen() { return laccForDgsCodegenLibraryAccessors; }

        /**
         * Returns the group of libraries at dgs.extended
         */
        public DgsExtendedLibraryAccessors getExtended() { return laccForDgsExtendedLibraryAccessors; }

        /**
         * Returns the group of libraries at dgs.spring
         */
        public DgsSpringLibraryAccessors getSpring() { return laccForDgsSpringLibraryAccessors; }

    }

    public static class DgsCodegenLibraryAccessors extends SubDependencyFactory {
        private final DgsCodegenClientLibraryAccessors laccForDgsCodegenClientLibraryAccessors = new DgsCodegenClientLibraryAccessors(owner);

        public DgsCodegenLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at dgs.codegen.client
         */
        public DgsCodegenClientLibraryAccessors getClient() { return laccForDgsCodegenClientLibraryAccessors; }

    }

    public static class DgsCodegenClientLibraryAccessors extends SubDependencyFactory {

        public DgsCodegenClientLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for core (com.netflix.graphql.dgs.codegen:graphql-dgs-codegen-client-core)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCore() { return create("dgs.codegen.client.core"); }

    }

    public static class DgsExtendedLibraryAccessors extends SubDependencyFactory {

        public DgsExtendedLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for scalars (com.netflix.graphql.dgs:graphql-dgs-extended-scalars)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getScalars() { return create("dgs.extended.scalars"); }

            /**
             * Creates a dependency provider for validation (com.netflix.graphql.dgs:graphql-dgs-extended-validation)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getValidation() { return create("dgs.extended.validation"); }

    }

    public static class DgsSpringLibraryAccessors extends SubDependencyFactory {
        private final DgsSpringBootLibraryAccessors laccForDgsSpringBootLibraryAccessors = new DgsSpringBootLibraryAccessors(owner);

        public DgsSpringLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at dgs.spring.boot
         */
        public DgsSpringBootLibraryAccessors getBoot() { return laccForDgsSpringBootLibraryAccessors; }

    }

    public static class DgsSpringBootLibraryAccessors extends SubDependencyFactory {

        public DgsSpringBootLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for starter (com.netflix.graphql.dgs:graphql-dgs-spring-boot-starter)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getStarter() { return create("dgs.spring.boot.starter"); }

    }

    public static class HibernateLibraryAccessors extends SubDependencyFactory {

        public HibernateLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for jpamodelgen (org.hibernate:hibernate-jpamodelgen)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getJpamodelgen() { return create("hibernate.jpamodelgen"); }

    }

    public static class Junit5LibraryAccessors extends SubDependencyFactory {

        public Junit5LibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for api (org.junit.jupiter:junit-jupiter-api)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getApi() { return create("junit5.api"); }

            /**
             * Creates a dependency provider for engine (org.junit.jupiter:junit-jupiter-engine)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getEngine() { return create("junit5.engine"); }

    }

    public static class KafkaLibraryAccessors extends SubDependencyFactory {

        public KafkaLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for clients (org.apache.kafka:kafka-clients)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getClients() { return create("kafka.clients"); }

    }

    public static class LogstashLibraryAccessors extends SubDependencyFactory {
        private final LogstashLogbackLibraryAccessors laccForLogstashLogbackLibraryAccessors = new LogstashLogbackLibraryAccessors(owner);

        public LogstashLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at logstash.logback
         */
        public LogstashLogbackLibraryAccessors getLogback() { return laccForLogstashLogbackLibraryAccessors; }

    }

    public static class LogstashLogbackLibraryAccessors extends SubDependencyFactory {

        public LogstashLogbackLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for encoder (net.logstash.logback:logstash-logback-encoder)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getEncoder() { return create("logstash.logback.encoder"); }

    }

    public static class ReactorLibraryAccessors extends SubDependencyFactory {

        public ReactorLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for core (io.projectreactor:reactor-core)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCore() { return create("reactor.core"); }

    }

    public static class SpringLibraryAccessors extends SubDependencyFactory {
        private final SpringBootLibraryAccessors laccForSpringBootLibraryAccessors = new SpringBootLibraryAccessors(owner);
        private final SpringCloudLibraryAccessors laccForSpringCloudLibraryAccessors = new SpringCloudLibraryAccessors(owner);
        private final SpringDataLibraryAccessors laccForSpringDataLibraryAccessors = new SpringDataLibraryAccessors(owner);

        public SpringLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at spring.boot
         */
        public SpringBootLibraryAccessors getBoot() { return laccForSpringBootLibraryAccessors; }

        /**
         * Returns the group of libraries at spring.cloud
         */
        public SpringCloudLibraryAccessors getCloud() { return laccForSpringCloudLibraryAccessors; }

        /**
         * Returns the group of libraries at spring.data
         */
        public SpringDataLibraryAccessors getData() { return laccForSpringDataLibraryAccessors; }

    }

    public static class SpringBootLibraryAccessors extends SubDependencyFactory {
        private final SpringBootGradleLibraryAccessors laccForSpringBootGradleLibraryAccessors = new SpringBootGradleLibraryAccessors(owner);

        public SpringBootLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for dependencies (org.springframework.boot:spring-boot-dependencies)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getDependencies() { return create("spring.boot.dependencies"); }

        /**
         * Returns the group of libraries at spring.boot.gradle
         */
        public SpringBootGradleLibraryAccessors getGradle() { return laccForSpringBootGradleLibraryAccessors; }

    }

    public static class SpringBootGradleLibraryAccessors extends SubDependencyFactory {

        public SpringBootGradleLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for plugin (org.springframework.boot:spring-boot-gradle-plugin)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getPlugin() { return create("spring.boot.gradle.plugin"); }

    }

    public static class SpringCloudLibraryAccessors extends SubDependencyFactory {

        public SpringCloudLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for dependencies (org.springframework.cloud:spring-cloud-dependencies)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getDependencies() { return create("spring.cloud.dependencies"); }

    }

    public static class SpringDataLibraryAccessors extends SubDependencyFactory {

        public SpringDataLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for mongodb (org.springframework.data:spring-data-mongodb)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getMongodb() { return create("spring.data.mongodb"); }

    }

    public static class VersionAccessors extends VersionFactory  {

        private final AxonVersionAccessors vaccForAxonVersionAccessors = new AxonVersionAccessors(providers, config);
        private final BenVersionAccessors vaccForBenVersionAccessors = new BenVersionAccessors(providers, config);
        private final ComVersionAccessors vaccForComVersionAccessors = new ComVersionAccessors(providers, config);
        private final CommonsVersionAccessors vaccForCommonsVersionAccessors = new CommonsVersionAccessors(providers, config);
        private final DgsVersionAccessors vaccForDgsVersionAccessors = new DgsVersionAccessors(providers, config);
        private final FlywaydbVersionAccessors vaccForFlywaydbVersionAccessors = new FlywaydbVersionAccessors(providers, config);
        private final HibernateVersionAccessors vaccForHibernateVersionAccessors = new HibernateVersionAccessors(providers, config);
        private final Junit5VersionAccessors vaccForJunit5VersionAccessors = new Junit5VersionAccessors(providers, config);
        private final KafkaVersionAccessors vaccForKafkaVersionAccessors = new KafkaVersionAccessors(providers, config);
        private final LogstashVersionAccessors vaccForLogstashVersionAccessors = new LogstashVersionAccessors(providers, config);
        private final LombokVersionAccessors vaccForLombokVersionAccessors = new LombokVersionAccessors(providers, config);
        private final MinioVersionAccessors vaccForMinioVersionAccessors = new MinioVersionAccessors(providers, config);
        private final OkhttpVersionAccessors vaccForOkhttpVersionAccessors = new OkhttpVersionAccessors(providers, config);
        private final PipelinrVersionAccessors vaccForPipelinrVersionAccessors = new PipelinrVersionAccessors(providers, config);
        private final PostgresqlVersionAccessors vaccForPostgresqlVersionAccessors = new PostgresqlVersionAccessors(providers, config);
        private final ReactorVersionAccessors vaccForReactorVersionAccessors = new ReactorVersionAccessors(providers, config);
        private final SpringVersionAccessors vaccForSpringVersionAccessors = new SpringVersionAccessors(providers, config);
        public VersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.axon
         */
        public AxonVersionAccessors getAxon() { return vaccForAxonVersionAccessors; }

        /**
         * Returns the group of versions at versions.ben
         */
        public BenVersionAccessors getBen() { return vaccForBenVersionAccessors; }

        /**
         * Returns the group of versions at versions.com
         */
        public ComVersionAccessors getCom() { return vaccForComVersionAccessors; }

        /**
         * Returns the group of versions at versions.commons
         */
        public CommonsVersionAccessors getCommons() { return vaccForCommonsVersionAccessors; }

        /**
         * Returns the group of versions at versions.dgs
         */
        public DgsVersionAccessors getDgs() { return vaccForDgsVersionAccessors; }

        /**
         * Returns the group of versions at versions.flywaydb
         */
        public FlywaydbVersionAccessors getFlywaydb() { return vaccForFlywaydbVersionAccessors; }

        /**
         * Returns the group of versions at versions.hibernate
         */
        public HibernateVersionAccessors getHibernate() { return vaccForHibernateVersionAccessors; }

        /**
         * Returns the group of versions at versions.junit5
         */
        public Junit5VersionAccessors getJunit5() { return vaccForJunit5VersionAccessors; }

        /**
         * Returns the group of versions at versions.kafka
         */
        public KafkaVersionAccessors getKafka() { return vaccForKafkaVersionAccessors; }

        /**
         * Returns the group of versions at versions.logstash
         */
        public LogstashVersionAccessors getLogstash() { return vaccForLogstashVersionAccessors; }

        /**
         * Returns the group of versions at versions.lombok
         */
        public LombokVersionAccessors getLombok() { return vaccForLombokVersionAccessors; }

        /**
         * Returns the group of versions at versions.minio
         */
        public MinioVersionAccessors getMinio() { return vaccForMinioVersionAccessors; }

        /**
         * Returns the group of versions at versions.okhttp
         */
        public OkhttpVersionAccessors getOkhttp() { return vaccForOkhttpVersionAccessors; }

        /**
         * Returns the group of versions at versions.pipelinr
         */
        public PipelinrVersionAccessors getPipelinr() { return vaccForPipelinrVersionAccessors; }

        /**
         * Returns the group of versions at versions.postgresql
         */
        public PostgresqlVersionAccessors getPostgresql() { return vaccForPostgresqlVersionAccessors; }

        /**
         * Returns the group of versions at versions.reactor
         */
        public ReactorVersionAccessors getReactor() { return vaccForReactorVersionAccessors; }

        /**
         * Returns the group of versions at versions.spring
         */
        public SpringVersionAccessors getSpring() { return vaccForSpringVersionAccessors; }

    }

    public static class AxonVersionAccessors extends VersionFactory  {

        private final AxonKafkaVersionAccessors vaccForAxonKafkaVersionAccessors = new AxonKafkaVersionAccessors(providers, config);
        private final AxonMongoVersionAccessors vaccForAxonMongoVersionAccessors = new AxonMongoVersionAccessors(providers, config);
        public AxonVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: axon.version (4.6.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("axon.version"); }

        /**
         * Returns the group of versions at versions.axon.kafka
         */
        public AxonKafkaVersionAccessors getKafka() { return vaccForAxonKafkaVersionAccessors; }

        /**
         * Returns the group of versions at versions.axon.mongo
         */
        public AxonMongoVersionAccessors getMongo() { return vaccForAxonMongoVersionAccessors; }

    }

    public static class AxonKafkaVersionAccessors extends VersionFactory  {

        public AxonKafkaVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: axon.kafka.version (4.6.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("axon.kafka.version"); }

    }

    public static class AxonMongoVersionAccessors extends VersionFactory  {

        public AxonMongoVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: axon.mongo.version (4.6.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("axon.mongo.version"); }

    }

    public static class BenVersionAccessors extends VersionFactory  {

        private final BenManesVersionAccessors vaccForBenManesVersionAccessors = new BenManesVersionAccessors(providers, config);
        public BenVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.ben.manes
         */
        public BenManesVersionAccessors getManes() { return vaccForBenManesVersionAccessors; }

    }

    public static class BenManesVersionAccessors extends VersionFactory  {

        private final BenManesCaffeineVersionAccessors vaccForBenManesCaffeineVersionAccessors = new BenManesCaffeineVersionAccessors(providers, config);
        public BenManesVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.ben.manes.caffeine
         */
        public BenManesCaffeineVersionAccessors getCaffeine() { return vaccForBenManesCaffeineVersionAccessors; }

    }

    public static class BenManesCaffeineVersionAccessors extends VersionFactory  {

        public BenManesCaffeineVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: ben.manes.caffeine.version (3.1.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("ben.manes.caffeine.version"); }

    }

    public static class ComVersionAccessors extends VersionFactory  {

        private final ComNetflixVersionAccessors vaccForComNetflixVersionAccessors = new ComNetflixVersionAccessors(providers, config);
        public ComVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.com.netflix
         */
        public ComNetflixVersionAccessors getNetflix() { return vaccForComNetflixVersionAccessors; }

    }

    public static class ComNetflixVersionAccessors extends VersionFactory  {

        private final ComNetflixDgsVersionAccessors vaccForComNetflixDgsVersionAccessors = new ComNetflixDgsVersionAccessors(providers, config);
        public ComNetflixVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.com.netflix.dgs
         */
        public ComNetflixDgsVersionAccessors getDgs() { return vaccForComNetflixDgsVersionAccessors; }

    }

    public static class ComNetflixDgsVersionAccessors extends VersionFactory  {

        private final ComNetflixDgsCodegenVersionAccessors vaccForComNetflixDgsCodegenVersionAccessors = new ComNetflixDgsCodegenVersionAccessors(providers, config);
        public ComNetflixDgsVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.com.netflix.dgs.codegen
         */
        public ComNetflixDgsCodegenVersionAccessors getCodegen() { return vaccForComNetflixDgsCodegenVersionAccessors; }

    }

    public static class ComNetflixDgsCodegenVersionAccessors extends VersionFactory  {

        public ComNetflixDgsCodegenVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: com.netflix.dgs.codegen.version (5.5.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("com.netflix.dgs.codegen.version"); }

    }

    public static class CommonsVersionAccessors extends VersionFactory  {

        private final CommonsCollections4VersionAccessors vaccForCommonsCollections4VersionAccessors = new CommonsCollections4VersionAccessors(providers, config);
        private final CommonsLang3VersionAccessors vaccForCommonsLang3VersionAccessors = new CommonsLang3VersionAccessors(providers, config);
        public CommonsVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.commons.collections4
         */
        public CommonsCollections4VersionAccessors getCollections4() { return vaccForCommonsCollections4VersionAccessors; }

        /**
         * Returns the group of versions at versions.commons.lang3
         */
        public CommonsLang3VersionAccessors getLang3() { return vaccForCommonsLang3VersionAccessors; }

    }

    public static class CommonsCollections4VersionAccessors extends VersionFactory  {

        public CommonsCollections4VersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: commons.collections4.version (4.4)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("commons.collections4.version"); }

    }

    public static class CommonsLang3VersionAccessors extends VersionFactory  {

        public CommonsLang3VersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: commons.lang3.version (3.12.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("commons.lang3.version"); }

    }

    public static class DgsVersionAccessors extends VersionFactory  {

        private final DgsCodegenVersionAccessors vaccForDgsCodegenVersionAccessors = new DgsCodegenVersionAccessors(providers, config);
        public DgsVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: dgs.version (5.3.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("dgs.version"); }

        /**
         * Returns the group of versions at versions.dgs.codegen
         */
        public DgsCodegenVersionAccessors getCodegen() { return vaccForDgsCodegenVersionAccessors; }

    }

    public static class DgsCodegenVersionAccessors extends VersionFactory  {

        private final DgsCodegenClientVersionAccessors vaccForDgsCodegenClientVersionAccessors = new DgsCodegenClientVersionAccessors(providers, config);
        public DgsCodegenVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.dgs.codegen.client
         */
        public DgsCodegenClientVersionAccessors getClient() { return vaccForDgsCodegenClientVersionAccessors; }

    }

    public static class DgsCodegenClientVersionAccessors extends VersionFactory  {

        public DgsCodegenClientVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: dgs.codegen.client.version (5.1.17)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("dgs.codegen.client.version"); }

    }

    public static class FlywaydbVersionAccessors extends VersionFactory  {

        public FlywaydbVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: flywaydb.version (9.6.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("flywaydb.version"); }

    }

    public static class HibernateVersionAccessors extends VersionFactory  {

        private final HibernateJpamodelgenVersionAccessors vaccForHibernateJpamodelgenVersionAccessors = new HibernateJpamodelgenVersionAccessors(providers, config);
        public HibernateVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.hibernate.jpamodelgen
         */
        public HibernateJpamodelgenVersionAccessors getJpamodelgen() { return vaccForHibernateJpamodelgenVersionAccessors; }

    }

    public static class HibernateJpamodelgenVersionAccessors extends VersionFactory  {

        public HibernateJpamodelgenVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: hibernate.jpamodelgen.version (5.6.8.Final)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("hibernate.jpamodelgen.version"); }

    }

    public static class Junit5VersionAccessors extends VersionFactory  {

        public Junit5VersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: junit5.version (5.8.2)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("junit5.version"); }

    }

    public static class KafkaVersionAccessors extends VersionFactory  {

        private final KafkaClientsVersionAccessors vaccForKafkaClientsVersionAccessors = new KafkaClientsVersionAccessors(providers, config);
        public KafkaVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.kafka.clients
         */
        public KafkaClientsVersionAccessors getClients() { return vaccForKafkaClientsVersionAccessors; }

    }

    public static class KafkaClientsVersionAccessors extends VersionFactory  {

        public KafkaClientsVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: kafka.clients.version (3.3.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("kafka.clients.version"); }

    }

    public static class LogstashVersionAccessors extends VersionFactory  {

        private final LogstashLogbackVersionAccessors vaccForLogstashLogbackVersionAccessors = new LogstashLogbackVersionAccessors(providers, config);
        public LogstashVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.logstash.logback
         */
        public LogstashLogbackVersionAccessors getLogback() { return vaccForLogstashLogbackVersionAccessors; }

    }

    public static class LogstashLogbackVersionAccessors extends VersionFactory  {

        private final LogstashLogbackEncoderVersionAccessors vaccForLogstashLogbackEncoderVersionAccessors = new LogstashLogbackEncoderVersionAccessors(providers, config);
        public LogstashLogbackVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.logstash.logback.encoder
         */
        public LogstashLogbackEncoderVersionAccessors getEncoder() { return vaccForLogstashLogbackEncoderVersionAccessors; }

    }

    public static class LogstashLogbackEncoderVersionAccessors extends VersionFactory  {

        public LogstashLogbackEncoderVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: logstash.logback.encoder.version (7.1.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("logstash.logback.encoder.version"); }

    }

    public static class LombokVersionAccessors extends VersionFactory  {

        public LombokVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: lombok.version (1.18.24)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("lombok.version"); }

    }

    public static class MinioVersionAccessors extends VersionFactory  {

        public MinioVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: minio.version (8.4.5)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("minio.version"); }

    }

    public static class OkhttpVersionAccessors extends VersionFactory  {

        public OkhttpVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: okhttp.version (4.10.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("okhttp.version"); }

    }

    public static class PipelinrVersionAccessors extends VersionFactory  {

        public PipelinrVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: pipelinr.version (0.5)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("pipelinr.version"); }

    }

    public static class PostgresqlVersionAccessors extends VersionFactory  {

        public PostgresqlVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: postgresql.version (42.5.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("postgresql.version"); }

    }

    public static class ReactorVersionAccessors extends VersionFactory  {

        public ReactorVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: reactor.version (3.4.24)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("reactor.version"); }

    }

    public static class SpringVersionAccessors extends VersionFactory  {

        private final SpringBootVersionAccessors vaccForSpringBootVersionAccessors = new SpringBootVersionAccessors(providers, config);
        private final SpringCloudVersionAccessors vaccForSpringCloudVersionAccessors = new SpringCloudVersionAccessors(providers, config);
        private final SpringDataVersionAccessors vaccForSpringDataVersionAccessors = new SpringDataVersionAccessors(providers, config);
        public SpringVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.spring.boot
         */
        public SpringBootVersionAccessors getBoot() { return vaccForSpringBootVersionAccessors; }

        /**
         * Returns the group of versions at versions.spring.cloud
         */
        public SpringCloudVersionAccessors getCloud() { return vaccForSpringCloudVersionAccessors; }

        /**
         * Returns the group of versions at versions.spring.data
         */
        public SpringDataVersionAccessors getData() { return vaccForSpringDataVersionAccessors; }

    }

    public static class SpringBootVersionAccessors extends VersionFactory  {

        public SpringBootVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: spring.boot.version (2.7.5)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("spring.boot.version"); }

    }

    public static class SpringCloudVersionAccessors extends VersionFactory  {

        public SpringCloudVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: spring.cloud.version (2021.0.4)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("spring.cloud.version"); }

    }

    public static class SpringDataVersionAccessors extends VersionFactory  {

        private final SpringDataMongodbVersionAccessors vaccForSpringDataMongodbVersionAccessors = new SpringDataMongodbVersionAccessors(providers, config);
        public SpringDataVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.spring.data.mongodb
         */
        public SpringDataMongodbVersionAccessors getMongodb() { return vaccForSpringDataMongodbVersionAccessors; }

    }

    public static class SpringDataMongodbVersionAccessors extends VersionFactory  {

        public SpringDataMongodbVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: spring.data.mongodb.version (3.4.5)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getVersion() { return getVersion("spring.data.mongodb.version"); }

    }

    public static class BundleAccessors extends BundleFactory {

        public BundleAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a dependency bundle provider for junit5 which is an aggregate for the following dependencies:
             * <ul>
             *    <li>org.junit.jupiter:junit-jupiter-api</li>
             *    <li>org.junit.jupiter:junit-jupiter-engine</li>
             * </ul>
             * This bundle was declared in catalog libs.versions.toml
             */
            public Provider<ExternalModuleDependencyBundle> getJunit5() { return createBundle("junit5"); }

    }

    public static class PluginAccessors extends PluginFactory {
        private final DgsPluginAccessors baccForDgsPluginAccessors = new DgsPluginAccessors(providers, config);

        public PluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of bundles at plugins.dgs
         */
        public DgsPluginAccessors getDgs() { return baccForDgsPluginAccessors; }

    }

    public static class DgsPluginAccessors extends PluginFactory {

        public DgsPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for dgs.codegen to the plugin id 'com.netflix.dgs.codegen'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getCodegen() { return createPlugin("dgs.codegen"); }

    }

}
